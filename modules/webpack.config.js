module.exports = {
    entry : "./src/js/index.js",
    output :{
        path: __dirname + "/src",
        filename:"budle.js"
    },


    module:{
        rules : [{
            test: /\.js$/,
            exclude : /node_modules/,
            use:{
                loader:"babel-loader",
                options:{
                    presets: ['es2015']
                }
            }
        }]
    }
}