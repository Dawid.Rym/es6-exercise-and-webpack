import $ from 'jquery'
class Person {
    constructor(firstName, lastName){
        this.firstName = firstName
        this.lastName = lastName
    }
    sayHello(){
        return `Czesc nazywam sie ${this.firstName} ${this.lastName}`
    }
}
let jan = new Person('Jan', 'Kowalski')

$('h1').html( jan.sayHello() )

export default Person;